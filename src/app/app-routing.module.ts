import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  //{ path : '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'home', loadChildren: './tab1/home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './tab1/login/login.module#LoginPageModule' },
  { path: 'gps', loadChildren: './gps/gps.module#GpsPageModule' },
  { path: 'map', loadChildren: './map/map.module#MapPageModule' },
  { path: 'parking-add', loadChildren: './parking-add/parking-add.module#ParkingAddPageModule' },
  { path: 'parking-detail', loadChildren: './parking-detail/parking-detail.module#ParkingDetailPageModule' },
  { path: 'qrcode', loadChildren: './qrcode/qrcode.module#QrcodePageModule' },
  { path: 'setting', loadChildren: './setting/setting.module#SettingPageModule' },
  { path: 'parking-list', loadChildren: './parking-list/parking-list.module#ParkingListPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
