import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  private user;
  constructor(private route:ActivatedRoute, private alertCon : AlertController,private toastCon : ToastController){
   
  }
  ngOnInit() {
     this.route.params.subscribe((data)=>{
      this.user = data;
      //console.log(this.user);
    });
  }
  showAlert()
  { this.alertCon.create({
      header: 'TestAlert',
      subHeader: 'เทสๆ',
      message : 'โหลเทส',
      buttons: ['OK']
    }).then((alert)=>{
      alert.present();
    })

  }
  showToast()
  {
    this.toastCon.create({
      message: 'Your settings have been saved.',
      duration: 3000
    }).then((toast)=>{
      toast.present();
    });
  }

}
