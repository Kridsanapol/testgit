import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private uname;
  private password;
  private user;
  constructor(private route : Router) { 
    this.user = {
      uname: '',
      password: 0
      };
  }

  ngOnInit() {
  }
  goLogin(){
    this.user.uname = this.uname;
    this.user.password = this.password;
    //console.log(this.user);
    this.route.navigate(['home', this.user]);

  }
}
